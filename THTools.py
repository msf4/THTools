#TurkHackTeam Ar-Ge Kulubu || THTools || v 1.0
#Herturlu Alip, Kullanip, Gelistirebilirsiniz Arkadaslar
# -*-coding:UTF-8-*-

"""Additions"""
from tkinter import *
from tkinter import messagebox,filedialog
import subprocess
import files
import img
import bbcode
import os

"""Class Area"""
class THTools(object):
    
    """Constructive Function"""
    def __init__(self):
        root.geometry("600x500")
        root.resizable(width=False,height=False)
        root.iconbitmap("img\\logoicon.ico")
        root.title(60*" "+"THTools | TurkHackTeam | Ar-Ge Kulubu")
        self.Images()
        self.Buttons()
        self.HelpButtons()

    """Images Area"""
    def Images(self):
        self._help1 = PhotoImage(file="img\\help.png")

        self._banner = PhotoImage(file = "img\\THTools.png")
        self._pht = Label(image=self._banner)
        self._pht.image = self._banner
        self._pht.place(relx= 0.1,rely= -0.0)

        self._framesql = PhotoImage(file="img\\buttonframe.png")
        self._pht1 = Label(image=self._framesql)
        self._pht1.image = self._framesql
        self._pht1.place(relx=0.05, rely=0.29)

        self._framenmp = PhotoImage(file="img\\buttonframe.png")
        self._pht2 = Label(image=self._framenmp)
        self._pht2.image = self._framenmp
        self._pht2.place(relx=0.05, rely=0.39)

        self._framebf = PhotoImage(file="img\\buttonframe.png")
        self._pht3 = Label(image=self._framebf)
        self._pht3.image = self._framebf
        self._pht3.place(relx=0.05, rely=0.49)

        self._frameknu = PhotoImage(file="img\\buttonframe.png")
        self._pht4 = Label(image=self._frameknu)
        self._pht4.image = self._frameknu
        self._pht4.place(relx=0.05, rely=0.59)

        self._framepy2 = PhotoImage(file="img\\buttonframe.png")
        self._pht5 = Label(image=self._framepy2)
        self._pht5.image = self._framepy2
        self._pht5.place(relx=0.05, rely=0.69)

        self._frameprsn = PhotoImage(file="img\\buttonframe.png")
        self._pht6 = Label(image=self._frameprsn)
        self._pht6.image = self._frameprsn
        self._pht6.place(relx=0.05, rely=0.79)

        self._framealty = PhotoImage(file="img\\buttonframe.png")
        self._pht7 = Label(image=self._framealty)
        self._pht7.image = self._framealty
        self._pht7.place(relx=0.05, rely=0.89)

        self._frameznmp = PhotoImage(file="img\\buttonframe.png")
        self._pht8 = Label(image=self._frameznmp)
        self._pht8.image = self._frameznmp
        self._pht8.place(relx=0.3, rely=0.29)

        self._frameap = PhotoImage(file="img\\buttonframe.png")
        self._pht9 = Label(image=self._frameap)
        self._pht9.image = self._frameap
        self._pht9.place(relx=0.3, rely=0.39)

        self._framews = PhotoImage(file="img\\buttonframe.png")
        self._pht10 = Label(image=self._framews)
        self._pht10.image = self._frameap
        self._pht10.place(relx=0.3, rely=0.49)

    """Buttons Area"""
    def Buttons(self):
        self._sqlmap = Button(
            text="Sqlmap",
            bg="#181818",
            fg="cyan",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationOne
        )
        self._sqlmap.place(relx=0.06,rely=0.3)

        self._nmap = Button(
            text="Nmap",
            bg="#181818",
            fg="lime",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationTwo
        )
        self._nmap.place(relx=0.06,rely=0.4)

        self._bruteforce = Button(
            text="BruteForce",
            bg="#181818",
            fg="magenta",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationThree
        )
        self._bruteforce.place(relx=0.06,rely=0.5)

        self._konu = Button(
            text="Konu Olusturucu",
            bg="#181818",
            fg="darkorange",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationFour
        )
        self._konu.place(relx=0.06,rely=0.6)

        self._py2exe = Button(
            text="Py2Exe",
            bg="#181818",
            fg="royalblue",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationFive
        )
        self._py2exe.place(relx=0.06,rely=0.7)

        self._personalize = Button(
            text="Personalize",
            bg="#181818",
            fg="orange",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationSix
        )
        self._personalize.place(relx=0.06,rely=0.8)

        self._altay = Button(
            text="Turk Altay",
            bg="#181818",
            fg="red",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationSeven

        )
        self._altay.place(relx=0.06,rely=0.9)

        self._zenmap = Button(
            text="Zenmap",
            bg="#181818",
            fg="yellowgreen",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationEight

        )
        self._zenmap.place(relx=0.31,rely=0.3)

        self._adminpnl = Button(
            text="AdminFinder",
            bg="#181818",
            fg="plum",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationNine

        )
        self._adminpnl.place(relx=0.31,rely=0.4)

        self._wireshark = Button(
            text="WireShark",
            bg="#181818",
            fg="palegreen",
            overrelief="groove",
            cursor="hand2",
            width=12,
            command=self.OperationTen

        )
        self._wireshark.place(relx=0.31,rely=0.5)

    """Buttons Commands"""
    def OperationOne(self):
        self.info = messagebox.showinfo("Uyarı",
                                        "Sqlmap Python 2 Sürümünde Çalışmaktadır!",
                                        detail="Python Sürümünüz 2x Değilse Lütfen Path Ayarlarını Değiştiriniz")
        subprocess.Popen('cmd.exe /K python2 files\\sqlmap-master\\sqlmap.py')


    def OperationTwo(self):
        subprocess.Popen('cmd.exe /K cd files\\Nmap')

    def OperationThree(self):
        self.info = messagebox.askokcancel("Uyarı",
                                           "Virüs Programınızı Devre Dışı Bırakınız",
                                           detail="Virüs Programınız Devre Dışı İse Tamam Tuşuna Tıklayınız.")
        if (self.info == True):
            subprocess.Popen('files\\BF\\bruteforce.exe')

    def OperationFour(self):
        subprocess.Popen('python files\\tht\\THT.py')

    def OperationFive(self):
        subprocess.Popen('python files\\Py2Exe\\Py2Exe.py')

    def OperationSix(self):
        subprocess.Popen('files\\Personalize02\\THTARGEPersonalize.exe')

    def OperationSeven(self):
        subprocess.Popen('files\\Altay\\altay.exe')

    def OperationEight(self):
        subprocess.Popen('files\\Nmap\\zenmap.exe')

    def OperationNine(self):
        self.info = messagebox.askokcancel("Uyarı",
                                           "Virüs Programınızı Devre Dışı Bırakınız",
                                           detail="Virüs Programınız Devre Dışı İse Tamam Tuşuna Tıklayınız.")
        if (self.info == True):
            subprocess.Popen('files\\adminpanel\\adminpanel.exe')

    def OperationTen(self):
        subprocess.Popen('files\\WiresharkPortable\\WiresharkPortable.exe')

    """Help Buttons Area"""
    def HelpButtons(self):

        self._helpButton1 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButOne
        )
        self._helpButton1.place(relx=0.23,rely=0.3)

        self._helpButton2 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButTwo
        )
        self._helpButton2.place(relx=0.23,rely=0.4)

        self._helpButton3 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButThree
        )
        self._helpButton3.place(relx=0.23,rely=0.5)

        self._helpButton4 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButFour
        )
        self._helpButton4.place(relx=0.23,rely=0.6)

        self._helpButton5 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButFive
        )
        self._helpButton5.place(relx=0.23,rely=0.7)

        self._helpButton6 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButSix
        )
        self._helpButton6.place(relx=0.23,rely=0.8)

        self._helpButton7 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButSeven
        )
        self._helpButton7.place(relx=0.23,rely=0.9)

        self._helpButton8 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButEight
        )
        self._helpButton8.place(relx=0.48,rely=0.3)

        self._helpButton9 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButNine
        )
        self._helpButton9.place(relx=0.48,rely=0.4)

        self._helpButton10 = Button(
            bg='#181818',
            overrelief="groove",
            cursor="hand2",
            image=self._help1,
            command=self.HelpButTen
        )
        self._helpButton10.place(relx=0.48,rely=0.5)

    """Help Buttons Commands"""

    #Sqlmap
    def HelpButOne(self):
        self._helpButOne = Toplevel()
        self._helpButOne.geometry("400x200")
        self._helpButOne.resizable(width=False,height=False)
        self._helpButOne.title("SqlMap Hakkında")
        self._helpButOne.iconbitmap("img\\helpicon.ico")

        self._labelSql1 = Label(
            self._helpButOne,
            text=
            """
            Sqlmap Nedir?
                Sqlmap python dili ile yazılmış açık kaynak kodlu,
                sql injection açığını tespit ve istismar eden bir araçtır.
                Bu araç sizin belirtmiş olduğunuz girdiler ile hedef site üzerinde,
                bünyesinde bulunan kombinasyonları deneyerek açık tarar.
            """,
            font="Times 10 bold",
            fg="cyan")
        self._labelSql1.place(relx=-0.1, rely=0.0)

        self._labelSql2 = Label(
            self._helpButOne,
            text="Kullanımı: sqlmap.py -u (site) [options]",
            font="Times 15 bold",
            fg="black"
        )
        self._labelSql2.place(relx=0.1,rely=0.6)

    #Nmap
    def HelpButTwo(self):
        self._helpButTwo = Toplevel()
        self._helpButTwo.geometry("400x200")
        self._helpButTwo.title("Nmap Hakkında")
        self._helpButTwo.iconbitmap("img\\helpicon.ico")
        self._labelNmp1 = Label(
            self._helpButTwo,
            text=
            """
            Nmap Nedir?
                Nmap ile kullanılan ağa bağlı herhangi bir makinenin,
                işletim sistemini, fiziksel aygıt tiplerini,
                çalışma süresini, yazılım bilgilerini,
                güvenlik duvarı ve ağ kartıyla ilgili bilgilerini,
                port servislerini ve port servislerinin versiyonlarını
                nmap ile öğrenebilirsiniz.
            """,
            font="Times 10 bold",
            fg="lime")
        self._labelNmp1.place(relx=0.0, rely=0.0)

        self._labelNmp2 = Label(
            self._helpButTwo,
            text="Kullanımı: nmap (options) [ip address]",
            font="Times 15 bold",
            fg="black"
        )
        self._labelNmp2.place(relx=0.1,rely=0.7)

    #BrueForce
    def HelpButThree(self):
        self._helpButThree = Toplevel()
        self._helpButThree.geometry("400x200")
        self._helpButThree.title("BruteForce Hakkında")
        self._helpButThree.iconbitmap("img\\helpicon.ico")
        self._labelBf1 = Label(
            self._helpButThree,
            text=
            """
            BruteForce Nedir?
            Brute Force, bir parolayı ele geçirmek için yapılan
            bir çeşit dijital  ve kriptografi saldırısıdır.
            Brute-Force tekniğinde elde herhangi bir bilgi
            bulunmuyor olmasına karşın belli şifreler denenerek
            doğru şifreye ulaşmaya çalışılır. Bu saldırı yönteminde
            genellikle insanların sıklıkla kullandığı basit şifreler başta
            olmak üzere birçok şifreden oluşan bir liste hazırlanır.
            Daha sonra meydana getirilen yardımcı bir yazılım yardımıyla
            veya el yordamıyla bu şifreler parolası bulunmak istenen hesaba
            giriş yapabilmek için tekrar tekrar denenir.
             Yazılım, doğru şifre bulunduğu anda bir sinyal vererek işlemi durdurur.
            """,
            font="Times 8 bold",
            fg="magenta")
        self._labelBf1.place(relx=-0.1, rely=0.0)

    #KonuOlusturucu
    def HelpButFour(self):
        self._helpButFour = Toplevel()
        self._helpButFour.geometry("400x200")
        self._helpButFour.title("KonuOlusturucu Hakkında")
        self._helpButFour.iconbitmap("img\\helpicon.ico")

        self._labelKo1 = Label(
            self._helpButFour,
            text=
            """
            Konu Olusturucu Nedir?
            Bu program sayesinde internetimizin olmadığı zamanlarda
            forumdaki konu olusturma seceneğindeki gibi konu olusturabilir,
            önizleyebilir ve işimiz bittiğinde kaydedip bir kenara saklayarak
            konumuzu hazırda tutabiliriz.
            """,
            font="Times 10 bold",
            fg="darkorange")
        self._labelKo1.place(relx=-0.05, rely=0.0)

        self._labelKo2 = Label(
            self._helpButFour,
            text="Program Yapımcısı: SessizEr ",
            font="Times 15 bold",
            fg="black"
        )
        self._labelKo2.place(relx=0.15,rely=0.5)

    #Py2Exe
    def HelpButFive(self):
        self._helpButFive = Toplevel()
        self._helpButFive.geometry("400x200")
        self._helpButFive.title("Py2Exe Hakkında")
        self._helpButFive.iconbitmap("img\\helpicon.ico")

        self._labelPe1 = Label(
            self._helpButFive,
            text=
            """
            Py2Exe Nedir?
            cx_Freeze programı yardımıyla var olan python dosyanıza
            ait sizin belirlediğiniz program adı, versiyonu,
            program açıklaması, iconunu otomatik olarak bir setup.py
            dosyası oluşturup içine girilen değerleri aktararak
            setup.py dosyasını otomatik olarak başlatıp
            build dosyası şeklinde .exe dosyanızı oluşturmaya yarıyor.
            """,
            font="Times 10 bold",
            fg="royalblue")
        self._labelPe1.place(relx=0.0, rely=0.0)

        self._labelPe2 = Label(
            self._helpButFive,
            text="Program Yapımcısı: SessizEr ",
            font="Times 15 bold",
            fg="black"
        )
        self._labelPe2.place(relx=0.15,rely=0.65)

    #Personalize
    def HelpButSix(self):
        self._helpButSix = Toplevel()
        self._helpButSix.geometry("400x200")
        self._helpButSix.title("Personalize Hakkında")
        self._helpButSix.iconbitmap("img\\helpicon.ico")

        self._labelPrsn1 = Label(
            self._helpButSix,
            text=
            """
            Personalize Nedir?
            turkhackteam.org uyesi olan Mocean tarafından
            yapılmış ve bilgisayarınızı kişileştirmeye,
            temizlemeye, hızlandırmaya ve bilgisayardaki
            problemleri çözmeye yarıyor.
            """,
            font="Times 10 bold",
            fg="sandybrown")
        self._labelPrsn1.place(relx=0.05, rely=0.0)

        self._labelPrsn2 = Label(
            self._helpButSix,
            text="Program Yapımcısı: Mocean ",
            font="Times 15 bold",
            fg="black"
        )
        self._labelPrsn2.place(relx=0.15,rely=0.65)

     #TurkAltay
    def HelpButSeven(self):
        self._helpButSeven = Toplevel()
        self._helpButSeven.geometry("400x200")
        self._helpButSeven.title("TurkAltay Hakkında")
        self._helpButSeven.iconbitmap("img\\helpicon.ico")

        self._labelTa1 = Label(
            self._helpButSeven,
            text=
            """
            Türk Altay Nedir?
            İçerisinde çeşitli araçlar olan;
            SQLi Scanner,Bruteforce,Arama Motoru,
            Ddos,Oto İndex, SQL Login Bypass,
            SQL Injection,Admin Panel Bulucu,
            Crawler, WHOIS, Reverse IP,
            Reverse DNS, IS IT DOWN,
            Port Kontrolü, PageRank bulunan
            işe yarar bir programdır.
            """,
            font="Times 10 bold",
            fg="red")
        self._labelTa1.place(relx=0.07, rely=0.0)

        self._labelTa2 = Label(
            self._helpButSeven,
            text="Program Yapımcısı: TurkHackTeam Ar-Ge Tim Lideri KeyLo99 ",
            font="Times 10 bold",
            fg="black"
        )
        self._labelTa2.place(relx=0.03,rely=0.85)

    #ZenMap
    def HelpButEight(self):
        self._helpButEight = Toplevel()
        self._helpButEight.geometry("400x200")
        self._helpButEight.title("ZenMap Hakkında")
        self._helpButEight.iconbitmap("img\\helpicon.ico")

        self._labelZm1 = Label(
            self._helpButEight,
            text=
            """
            ZenMap Nedir?
            Grafiksel kullanıcı arabiriminden faydalanılarak geliştirilmiş ,
            Nmap ın grafiksel arayüz halidir. Nmap'a başlayan kullanıcılar
            için ideal bir programdır. Aynı zamanda sık kullanılan taramaları,
             başka zaman açdığımızda tekrar tarayalım, kolayca işimizi halledelim
            diye profillere kayıt edilir.
            """,
            font="Times 10 bold",
            fg="yellowgreen")
        self._labelZm1.place(relx=-0.1, rely=0.0)

    #AdminFinder
    def HelpButNine(self):
        self._helpButNine = Toplevel()
        self._helpButNine.geometry("400x200")
        self._helpButNine.title("AdminFinder Hakkında")
        self._helpButNine.iconbitmap("img\\helpicon.ico")

        self._labelAp1 = Label(
            self._helpButNine,
            text=
            """
            AdminFinder Nedir?
            Sitelere ait olan admin panel girişlerini ister standart
            admin panel listeleriyle istersenizde sizin oluşturmuş olduğunuz
            admin panel listesiyle deneme yanılma yoluyla otomatik
            denenerek sizlerin karşısına çıkarır.
            """,
            font="Times 10 bold",
            fg="plum")
        self._labelAp1.place(relx=-0.04, rely=0.0)

    #WireShark
    def HelpButTen(self):
        self._helpButTen = Toplevel()
        self._helpButTen.geometry("400x200")
        self._helpButTen.title("WireShark Hakkında")
        self._helpButTen.iconbitmap("img\\helpicon.ico")

        self._labelWs1 = Label(
            self._helpButTen,
            text=
            """
            WireShark Nedir?
            Wireshark network trafiğinin,
            bir grafik arayüz üzerinden izlenmesini sağlayan,
            pek çok zaman hayat kurtarancı öneme sahip bir programdır.
            Uygulamanın kurulu olduğu bilgisayar üzerinden anlık network trafiği
            izlenebileceği gibi, Wireshark daha önce kaydedilmiş dosyaların
            incelenmesi amacı ile de kullanılabilir.
            """,
            font="Times 10 bold",
            fg="green")
        self._labelWs1.place(relx=-0.1, rely=0.0)

""""İdentifiers"""
root = Tk()
root.wait_visibility(root)
root.wm_attributes('-alpha',0.95)
thtools=THTools()
root.mainloop()
